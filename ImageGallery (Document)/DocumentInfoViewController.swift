//
//  DocumentInfoViewController.swift
//  ImageGallery (Document)
//
//  Created by zweqi on 01/09/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

class DocumentInfoViewController: UIViewController {

    var document: ImageGalleryDocument? {
        // updating UI before outlets get set (may be preparing or something)
        didSet { updateUI() }
    }
    
    // MARK: - View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // setting up the smallest possible size as a default size for a popover VC
        if let fittedSize = topLevelView?.sizeThatFits(UILayoutFittingCompressedSize) {
            preferredContentSize = CGSize(width: fittedSize.width + 30, height: fittedSize.height + 30)
        }
    }

    private let shortDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .short
        return formatter
    }()

    var isEmbedded = false
    
    // updating thumbnail image and labels with document attributes
    private func updateUI() {
        if isEmbedded {
            sizeLabel.text = "Unknown"
            createdLabel.text = "Unknown"
            return
        }
        
        if sizeLabel != nil, createdLabel != nil,
            let url = document?.fileURL,
            let attributes = try? FileManager.default.attributesOfItem(atPath: url.path) {
            
            sizeLabel.text = "\(attributes[.size] ?? 0) bytes"
            if let created = attributes[.creationDate] as? Date {
                createdLabel.text = shortDateFormatter.string(from: created)
            }
            if thumbnailImageView != nil, thumbnailAspectRatio != nil, let thumbnail = document?.thumbnail {
                thumbnailImageView.image = thumbnail
                
                // changing aspect ratio constraint by removing/adding a new one
                thumbnailImageView.removeConstraint(thumbnailAspectRatio)
                thumbnailAspectRatio = NSLayoutConstraint(
                    item: thumbnailImageView,
                    attribute: .width,
                    relatedBy: .equal,
                    toItem: thumbnailImageView,
                    attribute: .height,
                    multiplier: thumbnail.size.width / thumbnail.size.height,
                    constant: 0
                )
                thumbnailImageView.addConstraint(thumbnailAspectRatio)
            }
            
            // clearing up a space for popover VC
            if presentationController is UIPopoverPresentationController {
                thumbnailImageView?.isHidden = true
                returnToDocumentButton?.isHidden = true
                view.backgroundColor = .clear
            }
        }
    }
    
    // MARK: - Storyboard

    @IBOutlet weak var returnToDocumentButton: UIButton!
    @IBOutlet weak var topLevelView: UIStackView!
    @IBOutlet weak var thumbnailAspectRatio: NSLayoutConstraint!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var createdLabel: UILabel!
    
    @IBAction func done() {
        presentingViewController?.dismiss(animated: true)
    }
}
