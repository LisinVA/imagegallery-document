//
//  ImageViewController.swift
//  ImageGallery
//
//  Created by zweqi on 29/06/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {

    // MARK: - Scroll View
    
    @IBOutlet weak var scrollViewWidth: NSLayoutConstraint!
    @IBOutlet weak var scrollViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            guard backgroundImage != nil else { return }
            scrollView.minimumZoomScale = 0.1
            scrollView.maximumZoomScale = 5.0
            scrollView.delegate = self
            scrollView.addSubview(imageView)
        }
    }
    
    private func updateScrollView(with size: CGSize) {
        scrollView?.contentSize = size
        scrollViewWidth?.constant = size.width
        scrollViewHeight?.constant = size.height
    }
    
    // MARK: - Drawing the background
    
    private var imageView = ImageView()
    
    var backgroundImage: UIImage? {
        get {
            return imageView.backgroundImage
        }
        set {
            scrollView?.zoomScale = 1.0
            imageView.backgroundImage = newValue
            let size = newValue?.size ?? CGSize.zero
            updateScrollView(with: size)
        }
    }
    
    // MARK: - Embedded Document Info VC
    
    private var embeddedDocInfo: DocumentInfoViewController?
    
    @IBOutlet weak var embeddedDocInfoWidth: NSLayoutConstraint!
    @IBOutlet weak var embeddedDocInfoHeight: NSLayoutConstraint!

    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Embed Document Info" {
            embeddedDocInfo = segue.destination as? DocumentInfoViewController
            embeddedDocInfo?.isEmbedded = true
        }
    }
    
    // MARK: - View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let size = imageView.backgroundImage?.size ?? CGSize.zero
        updateScrollView(with: size)
    }
}

// MARK: - UIScrollViewDelegate

extension ImageViewController: UIScrollViewDelegate {
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        scrollViewHeight.constant = scrollView.contentSize.height
        scrollViewWidth.constant = scrollView.contentSize.width
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}
