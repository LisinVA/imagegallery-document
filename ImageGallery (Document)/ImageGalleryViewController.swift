//
//  ViewController.swift
//  ImageGallery
//
//  Created by zweqi on 15/06/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

class ImageGalleryViewController: UIViewController, UICollectionViewDelegate, UIPopoverPresentationControllerDelegate {
    
    // MARK: - Model
    
    var imageGallery = [ImageInfo]() {
        didSet {
            let isGalleryChanged = !oldValue.elementsEqual(imageGallery) {
                $0.url == $1.url && $0.aspectRatio == $1.aspectRatio
            }
            if isGalleryChanged {
                documentChanged()
            }
        }
    }
    
    // MARK: - Document Handling
    var document: ImageGalleryDocument?

    func documentChanged() {
        // update the document's Model to match ours
        document?.imageGallery = ImageGallery(images: imageGallery)
        // then tell the document that something has changed, so it will trigger autosave
        if document?.imageGallery != nil {
            document?.updateChangeCount(.done)
        }
    }
    
    @IBAction func close(_ sender: UIBarButtonItem? = nil) {
        // set a nice thumbnail instead of an icon for our document
        if document?.imageGallery != nil {
            document?.thumbnail = imageCollectionView.snapshot
        }
        dismiss(animated: true) {
            self.document?.close()
        }
    }
    
    // MARK: - Storyboard
    
    @IBOutlet weak var imageCollectionView: UICollectionView! {
        didSet {
            imageCollectionView.dataSource = self
            imageCollectionView.delegate = self
            imageCollectionView.dragDelegate = self
            imageCollectionView.dropDelegate  = self
            imageCollectionView.dragInteractionEnabled = true   // by default it's disabled on iPhones
            addPinchGesture()
        }
    }
    
    @IBOutlet weak var trashButton: UIBarButtonItem!
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Show Document Info" {
            if let documentInfoVC = segue.destination.contents as? DocumentInfoViewController {
                document?.thumbnail = imageCollectionView.snapshot
                documentInfoVC.document = document
                if let ppc = documentInfoVC.popoverPresentationController {
                    ppc.delegate = self
                }
            }
        } else if segue.identifier == "ShowImage" {
            if let imageVC = segue.destination as? ImageViewController,
                let imageCell = sender as? ImageGalleryCollectionViewCell {
                imageVC.backgroundImage = imageCell.backgroundImage
            }
        }
    }
    
    // set to .none, so on iPhones we could see a popover VC too
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }

    @IBAction func close(bySegue: UIStoryboardSegue) {
        close()
    }

    // MARK: - Private Properties
    
    private var cellScale: CGFloat = 1.0
    private var indexPathsForDragging: [IndexPath] = []
    private var suppressBadURLWarnings = false
    
    // MARK: - View Controller Lifecycle

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // .normal will happen when we're coming back from Image View Controller
        // and we don't wanna reopen the whole document in that case
        guard document?.documentState != .normal else { return }

        document?.open { success in
            if success {
                self.title = self.document?.localizedName   // localizedName comes from url
                // update our Model from the document's Model
                self.imageGallery = self.document?.imageGallery?.images ?? [ImageInfo]()
                self.imageCollectionView.reloadData()
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        navigationController?.navigationBar.addInteraction(UIDropInteraction(delegate: self))        
    }
}

// MARK: - UICollectionViewDataSource

extension ImageGalleryViewController: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageGallery.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath)
        if let cell = cell as? ImageGalleryCollectionViewCell {
            let url = imageGallery[indexPath.item].url
            // TODO: move updateImageForCell() to the ImageGalleryCollectionViewCell
            updateImageForCell(cell, imageURL: url)
            addTapGesture(to: cell)
            flowLayout?.invalidateLayout()  // re-layout cells, adapting to their new height
        }
        return cell
    }
    
    private var font: UIFont {
        return UIFontMetrics(forTextStyle: .body).scaledFont(for: UIFont.preferredFont(forTextStyle: .body).withSize(29.0))
    }

    /// Fetching an image from the given URL and setting it as a 'cell.backgroundImage' value.
    ///
    /// If an image couldn't be fetched, nil value would be set for a 'cell.backgroundImage'.
    ///
    /// - Parameters:
    ///   - cell: The cell for which the 'backgroundImage' will be updated.
    ///   - imageURL: The URL of the image.
    private func updateImageForCell(_ cell: ImageGalleryCollectionViewCell, imageURL url: URL) {
        
        var cache = URLCache.shared
        cache = URLCache(memoryCapacity: 100 * 1024 * 1024, diskCapacity: 100 * 1024 * 1024, diskPath: nil)
        let request = URLRequest(url: url, cachePolicy: .returnCacheDataElseLoad)
        
        if let cachedResponse = cache.cachedResponse(for: request) {
            // there is a cached image, using it
            cell.backgroundImage = UIImage(data: cachedResponse.data)
            cell.label.isHidden = true
        } else {
            // there is no cached image, so we hitting URL for one
            cell.backgroundImage = nil
            cell.spinner.startAnimating()
            
            let session = URLSession(configuration: .default)
            DispatchQueue.global(qos: .userInitiated).async {
                let task = session.dataTask(with: request) { (data, response, error) in
                    DispatchQueue.main.async { [unowned self] in
                        if let data = data, let response = response, error == nil {
                            // got our image from the server, need to cache it
                            let cachedResponse = CachedURLResponse(response: response, data: data)
                            cache.storeCachedResponse(cachedResponse, for: request)
                            
                            cell.backgroundImage = UIImage(data: data)
                            cell.label.isHidden = true
                        } else {
                            // failed to load an image, using "placeholder" instead
                            let text = NSAttributedString(string: "No Image", attributes: [.font: self.font])
                            cell.label.attributedText = text
                            cell.label.isHidden = false
                            self.presentBadURLWarnings(for: url)
                        }
                        cell.spinner.stopAnimating()
                    }
                }
                task.resume()   // starting the task we've just created
            }
        }
    }
    
    private func presentBadURLWarnings(for url: URL) {
        guard !suppressBadURLWarnings else { return }
        
        let alert = UIAlertController(
            title: "Image Transfer Failed",
            message: "Couldn't transfer the dropped image from its source.\nShow this warning in the future?",
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(
            title: "Keep Warning",
            style: .default
        ))
        
        alert.addAction(UIAlertAction(
            title: "Stop Warning",
            style: .destructive,
            handler: { action in
                self.suppressBadURLWarnings = true
            }
        ))
        
        present(alert, animated: true)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension ImageGalleryViewController: UICollectionViewDelegateFlowLayout {

    var flowLayout: UICollectionViewFlowLayout? {
        return imageCollectionView?.collectionViewLayout as? UICollectionViewFlowLayout
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let imageAspectRatio = imageGallery[indexPath.item].aspectRatio
        let cellWidth = ImageGalleryCollectionViewCell.defaultWidth * cellScale
        let cellHeight = cellWidth * imageAspectRatio
        return CGSize(width: cellWidth, height: cellHeight)
    }
}

// MARK: - UICollectionViewDragDelegate

extension ImageGalleryViewController: UICollectionViewDragDelegate {

    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        
        session.localContext = collectionView
        indexPathsForDragging = []
        return dragItems(at: indexPath)
    }

    func collectionView(_ collectionView: UICollectionView, itemsForAddingTo session: UIDragSession, at indexPath: IndexPath, point: CGPoint) -> [UIDragItem] {
        return dragItems(at: indexPath)
    }
    
    /// Returns a drag item, holding URL of 'backgroundImage' of the cell at the given index path in the image collection view.
    ///
    /// Drag item also stores image URL in its 'localObject' property.
    ///
    /// - Parameters:
    ///   - indexPath: The index path of the cell being dragged.
    private func dragItems(at indexPath: IndexPath) -> [UIDragItem] {
        guard imageCollectionView.cellForItem(at: indexPath) is ImageGalleryCollectionViewCell else { return [] }
        
        indexPathsForDragging += [indexPath]
        let imageURL = imageGallery[indexPath.item].url as NSURL
        let dragItem = UIDragItem(itemProvider: NSItemProvider(object: imageURL))
        dragItem.localObject = imageURL
        return [dragItem]
    }
}

// MARK: - UICollectionViewDropDelegate

extension ImageGalleryViewController: UICollectionViewDropDelegate {

    func collectionView(_ collectionView: UICollectionView, canHandle session: UIDropSession) -> Bool {
        let isSelf = (session.localDragSession?.localContext as? UICollectionView) == collectionView
        if isSelf {
            // for local: accept drops of items that have NSURL representation
            return session.canLoadObjects(ofClass: NSURL.self)
        } else {
            // for non-local: accept drops of items that have both NSURL and UIImage representations
            return session.canLoadObjects(ofClass: NSURL.self) && session.canLoadObjects(ofClass: UIImage.self)
        }
    }

    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {

        // copy dragItems that go outside the app and move those that stay inside
        let isSelf = (session.localDragSession?.localContext as? UICollectionView) == collectionView
        return UICollectionViewDropProposal(operation: isSelf ? .move : .copy, intent: .insertAtDestinationIndexPath)
    }

    // performing a drop operation
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        let section = collectionView.numberOfSections - 1
        let item = collectionView.numberOfItems(inSection: section)
        let destinationIndexPath = coordinator.destinationIndexPath ?? IndexPath(item: item, section: section)

        for item in coordinator.items {
            if let sourceIndexPath = item.sourceIndexPath {
                // local drag and drop
                moveItem(item, from: sourceIndexPath, to: destinationIndexPath, withCoordinator: coordinator, inCollectionView: collectionView)
            } else {
                // non-local drag and drop
                insertItem(item, at: destinationIndexPath, withCoordinator: coordinator, inCollectionView: collectionView)
            }
        }
    }
    
    /// This method moves a cell from 'sourceIndexPath' to 'destinationIndexPath' within the same collection view.
    ///
    /// - Parameters:
    ///   - item: The item being dragged.
    ///   - sourceIndexPath: The index path of the item in the collection view.
    ///   - destinationIndexPath: The index path in the collection view at which the content would be dropped.
    ///   - coordinator: The coordinator object, obtained from performDropWith: UICollectionViewDropDelegate method.
    ///   - collectionView: The collection view in which moving needs to be done.
    private func moveItem(_ item: UICollectionViewDropItem, from sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath, withCoordinator coordinator: UICollectionViewDropCoordinator, inCollectionView collectionView: UICollectionView) {
        
        if let imageURL = item.dragItem.localObject as? URL {
            let imageAspectRatio = imageGallery[sourceIndexPath.item].aspectRatio
            let imageInfo = ImageInfo(url: imageURL, aspectRatio: imageAspectRatio)
            
            collectionView.performBatchUpdates({
                imageGallery.remove(at: sourceIndexPath.item)
                imageGallery.insert(imageInfo, at: destinationIndexPath.item)
                collectionView.deleteItems(at: [sourceIndexPath])
                collectionView.insertItems(at: [destinationIndexPath])
            })
            coordinator.drop(item.dragItem, toItemAt: destinationIndexPath)
        }
    }

    /// This method inserts a new cell at the 'destinationIndexPath' in the given collection view, based on an 'item' being dragged from another app.
    ///
    /// - Parameters:
    ///   - item: The item being dragged.
    ///   - destinationIndexPath: The index path in the collection view at which the content would be dropped.
    ///   - coordinator: The coordinator object, obtained from performDropWith: UICollectionViewDropDelegate method.
    ///   - collectionView: The collection view in which inserting needs to be done.
    private func insertItem(_ item: UICollectionViewDropItem, at destinationIndexPath: IndexPath, withCoordinator coordinator: UICollectionViewDropCoordinator, inCollectionView collectionView: UICollectionView) {
        
        // putting an instant placeholder, because the drop may take some time
        let placeholderContext = coordinator.drop(
            item.dragItem,
            to: UICollectionViewDropPlaceholder(insertionIndexPath: destinationIndexPath, reuseIdentifier: "DropPlaceholderCell")
        )
        
        // creating a dispatch group to start tasks concurrently and then get notified when they're finished
        let imagePropertiesGroup = DispatchGroup()

        // getting image URL from the dropping item
        var dropURL: URL?
        
        imagePropertiesGroup.enter()
        item.dragItem.itemProvider.loadObject(ofClass: NSURL.self) { (provider, error) in
            if let url = provider as? URL {
                dropURL = url.imageURL
            } else {
                print("ERROR: Failed to load URL: \(error?.localizedDescription ?? "Uknown reason.")" )
                placeholderContext.deletePlaceholder()
            }
            imagePropertiesGroup.leave()
        }
        
        // getting image from the dropping item
        var dropImage: UIImage?
        
        imagePropertiesGroup.enter()
        item.dragItem.itemProvider.loadObject(ofClass: UIImage.self) { (provider, error) in
            if let image = provider as? UIImage {
                dropImage = image
            } else {
                print("ERROR: Failed to load Image: \(error?.localizedDescription ?? "Uknown reason.")" )
                placeholderContext.deletePlaceholder()
            }
            imagePropertiesGroup.leave()
        }
        
        // updating data source with image and image URL
        imagePropertiesGroup.notify(queue: DispatchQueue.main) { [weak self] in
            let imageInfo = ImageInfo(url: dropURL!, aspectRatio: dropImage!.aspectRatio)
            placeholderContext.commitInsertion(dataSourceUpdates: { insertionIndexPath in
                self?.imageGallery.insert(imageInfo, at: insertionIndexPath.item)
            })
        }
    }
}

// MARK: - UIDropInteractionDelegate

extension ImageGalleryViewController: UIDropInteractionDelegate {
 
    func dropInteraction(_ interaction: UIDropInteraction, canHandle session: UIDropSession) -> Bool {
        return session.canLoadObjects(ofClass: NSURL.self)
    }
    
    func dropInteraction(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
        
        guard let trashButtonView = trashButton.value(forKey: "view") as? UIView else { return UIDropProposal(operation: .cancel) }
        
        let currentDropPoint = session.location(in: trashButtonView)
        let isDraggingOverTrashButton = trashButtonView.bounds.contains(currentDropPoint)
        return UIDropProposal(operation: isDraggingOverTrashButton ? .move : .cancel)
    }
    
    func dropInteraction(_ interaction: UIDropInteraction, performDrop session: UIDropSession) {
        indexPathsForDragging.sorted(by: >).forEach { indexPath in
            imageGallery.remove(at: indexPath.item)
        }
        imageCollectionView.deleteItems(at: indexPathsForDragging)
    }
}

// MARK: - Gestures

extension ImageGalleryViewController {
    
    /// Adds pinch gesture to the current image collection view.
    ///
    /// Pinch allows the user to change scale of width of the cells in this collection view.
    private func addPinchGesture() {
        let pinch = UIPinchGestureRecognizer(
            target: self,
            action: #selector(pinch(recognizer:))
        )
        imageCollectionView.addGestureRecognizer(pinch)
    }
    
    /// Changes scale of width of cells in image collection view.
    ///
    /// - Parameters:
    ///   - recognizer: The gesture recognizer provided by pinch gesture.
    @objc private func pinch(recognizer: UIPinchGestureRecognizer) {
        switch recognizer.state {
        case .changed:
            cellScale = recognizer.scale
            flowLayout?.invalidateLayout()
        case .ended:
            //saving new size of the cell
            if cellScale != 1.0 {
                ImageGalleryCollectionViewCell.defaultWidth *= cellScale
                cellScale = 1.0
                imageCollectionView.reloadData()
            }
        default:
            return
        }
    }
    
    /// Adds a single tap gesture to the image gallery collection view cell.
    ///
    /// Tap on cell opens a new MVC, which presents the background image of this cell
    /// - Parameters:
    ///   - cell: The cell to which tap gesture is being added.
    private func addTapGesture(to cell: ImageGalleryCollectionViewCell) {
        let tapGesture = UITapGestureRecognizer(
            target: self,
            action: #selector(tap(recognizer:))
        )
        cell.addGestureRecognizer(tapGesture)
    }
    
    /// Opens a new MVC, which presents the background image of this cell
    ///
    /// - Parameters:
    ///   - recognizer: The gesture recognizer provided by tap gesture.
    @objc private func tap(recognizer: UITapGestureRecognizer) {
        if let indexPath = imageCollectionView.indexPathForItem(at: recognizer.location(in: imageCollectionView)) {
            let cell = imageCollectionView.cellForItem(at: indexPath) as! ImageGalleryCollectionViewCell
            
            guard cell.backgroundImage != nil else { return }
            performSegue(withIdentifier: "ShowImage", sender: cell)
        }
    }
}
