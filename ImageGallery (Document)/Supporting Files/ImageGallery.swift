//
//  ImageGallery.swift
//  ImageGallery (Document)
//
//  Created by zweqi on 09/08/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

struct ImageInfo: Codable {
    let url: URL
    let aspectRatio: CGFloat    // image height to width ratio
}

// the Model for an Image Gallery Document

struct ImageGallery: Codable {
    
    var images = [ImageInfo]()
    
    // return this ImageGallery as a JSON data
    var json: Data? {
        return try? JSONEncoder().encode(self)
    }
    
    init(images: [ImageInfo]) {
        self.images = images
    }
    
    // take some JSON and try to init an ImageGallery from it
    init?(json: Data) {
        if let newValue = try? JSONDecoder().decode(ImageGallery.self, from: json) {
            self = newValue
        } else {
            return nil
        }
    }
}
