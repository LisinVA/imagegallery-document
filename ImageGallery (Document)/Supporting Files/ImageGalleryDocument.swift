//
//  ImageGalleryDocument.swift
//  ImageGallery (Document)
//
//  Created by zweqi on 09/08/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

class ImageGalleryDocument: UIDocument {
    
    var imageGallery: ImageGallery?     // the Model for this Document
    var thumbnail: UIImage?             // thumbnail image for this Document
    
    // turn the Model into a JSON Data
    override func contents(forType typeName: String) throws -> Any {
        return imageGallery?.json ?? Data()
    }
    
    // turn a JSON Data into the Model
    override func load(fromContents contents: Any, ofType typeName: String?) throws {
        if let json = contents as? Data {
            imageGallery = ImageGallery(json: json)
        }
    }
    
    // the added key-value pair sets a thumbnail UIImage for the UIDocument
    override func fileAttributesToWrite(to url: URL, for saveOperation: UIDocumentSaveOperation) throws -> [AnyHashable : Any] {
        var attributes = try super.fileAttributesToWrite(to: url, for: saveOperation)
        if let thumbnail = self.thumbnail {
            attributes[URLResourceKey.thumbnailDictionaryKey] =
                [URLThumbnailDictionaryItem.NSThumbnail1024x1024SizeKey: thumbnail]
        }
        return attributes
    }
}

