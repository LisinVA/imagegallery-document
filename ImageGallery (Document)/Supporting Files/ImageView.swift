//
//  ImageView.swift
//  ImageGallery
//
//  Created by zweqi on 29/06/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

class ImageView: UIView {
    
    var backgroundImage: UIImage? {
        didSet {
            let size = backgroundImage?.size ?? CGSize.zero
            frame = CGRect(origin: CGPoint.zero, size: size)
            setNeedsDisplay()
        }
    }
    
    override func draw(_ rect: CGRect) {
        backgroundImage?.draw(in: bounds)
    }
}
